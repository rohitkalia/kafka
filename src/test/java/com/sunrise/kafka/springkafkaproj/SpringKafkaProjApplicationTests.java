package com.sunrise.kafka.springkafkaproj;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.TestPropertySource;

import com.sunrise.kafka.domain.EbillEvent;
import com.sunrise.kafka.domain.OrderItem;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = { "test" })
@TestPropertySource(properties = { "spring.kafka.producer.bootstrap-servers=${spring.embedded.kafka.brokers}",
		"spring.kafka.admin.properties.bootstrap.servers=${spring.embedded.kafka.brokers}" })
class SpringKafkaProjApplicationTests {

	@Autowired
	TestRestTemplate testRestTempalte;

	private Consumer<Integer, String> consumer;

	@Autowired
	EmbeddedKafkaBroker embeddedKafkaBroker;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@BeforeEach
	private void setup() {
		Map<String, Object> config = new HashMap<>(KafkaTestUtils.consumerProps("group1", "true", embeddedKafkaBroker));
		consumer = new DefaultKafkaConsumerFactory(config, new IntegerDeserializer(), new StringDeserializer())
				.createConsumer();
		embeddedKafkaBroker.consumeFromAllEmbeddedTopics(consumer);
	}

	@AfterEach
	private void tearDow() {
		consumer.close();
	}

	@Test
	@Timeout(20)
	void simpleTest() {

		OrderItem orderItem = OrderItem.builder().invoiceAmt(123).invoiceID(123123).orderID("H2342").build();

		EbillEvent event = EbillEvent.builder().emailEventId(123123).ebppEvent(orderItem).build();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<EbillEvent> requestEntity = new HttpEntity<>(event, httpHeaders);
		ResponseEntity<EbillEvent> reponse = testRestTempalte.exchange("/kafka/default/ebppEvent", HttpMethod.POST,
				requestEntity, EbillEvent.class);

		assertEquals(HttpStatus.CREATED, reponse.getStatusCode());

		ConsumerRecord<Integer, String> consumerRecord = KafkaTestUtils.getSingleRecord(consumer, "test");
		String expected = "{\"emailEventId\":123123,\"ebppEvent\":{\"orderID\":\"H2342\",\"invoiceID\":123123,\"invoiceAmt\":123}}";
		String actual = consumerRecord.value();
		assertEquals(expected, actual);

	}

}
