package com.sunrise.kafka.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TopicUnknown extends RuntimeException {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1106763450401862123L;

public TopicUnknown(String message) {
    super(message);
  }

  public TopicUnknown(String message, Throwable cause) {
    super(message, cause);
  }
}
