package com.sunrise.kafka.springkafkaproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(
        basePackages = {
            "com.sunrise.kafka"
        },
        basePackageClasses = {com.sunrise.kafka.springkafkaproj.SpringBootWithKafkaApplication.class})
public class SpringBootWithKafkaApplication {	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithKafkaApplication.class, args);
	}

}
