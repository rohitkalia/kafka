package com.sunrise.kafka.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderItem {

	private String orderID;
	private Integer invoiceID;
	private Integer invoiceAmt;

}
