package com.sunrise.kafka.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EbillEvent {

	private Integer emailEventId;
	@NotNull
	@Valid
	private OrderItem ebppEvent;

}
