package com.sunrise.kafka.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sunrise.kafka.domain.EbillEvent;
import com.sunrise.kafka.service.producer.ProducerServiceImpl;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {
	private static final Logger logger = LoggerFactory.getLogger(KafkaController.class);

	@Autowired
	ProducerServiceImpl producer;

	@PostMapping(value = "/default/ebppEvent")
	public ResponseEntity<@Valid EbillEvent> sendMessageToKafkaDefaultTopic(@RequestBody @Valid EbillEvent event) throws JsonProcessingException {
	
			producer.sendMessageDefault(event);
			logger.info("Message Sent");	
			return ResponseEntity.status(HttpStatus.CREATED).body(event);
	
	}

	@PostMapping(value = "/async/ebppEvent")
	public  ResponseEntity<@Valid EbillEvent> sendMessageToKafkaTopicAsync(@RequestBody @Valid EbillEvent event) throws JsonProcessingException {
	
			producer.sendMessageToTheTopicAsync(event, "test");
			return ResponseEntity.status(HttpStatus.CREATED).body(event);
	}

	@PostMapping(value = "/sync/ebppEvent")
	public  ResponseEntity<@Valid EbillEvent> sendMessageToKafkaTopicSync(@RequestBody @Valid EbillEvent event) throws JsonProcessingException {
	
			producer.sendMessageToTheTopicSync(event, "test");			
			return ResponseEntity.status(HttpStatus.CREATED).body(event);
	}
	
	
	@PostMapping(value = "/pushRecords")
	public  ResponseEntity<@Valid EbillEvent> sendMessageToKafkaTopic(@RequestBody @Valid EbillEvent event) throws JsonProcessingException {
	
			producer.sendMessageToTheTopicWithProdcuerRecord(event, "test");			
			return ResponseEntity.status(HttpStatus.CREATED).body(event);
	}
}