package com.sunrise.kafka.service.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

//@Service
@Slf4j
public class ConsumerManualCommit implements AcknowledgingMessageListener<Integer, String> {
	
	
	@Override
	@KafkaListener(topics = "test")
	public void onMessage(ConsumerRecord<Integer, String> data, Acknowledgment acknowledgment) {
		log.info(" Record" +  data);	
		acknowledgment.acknowledge();	
		
	}

}