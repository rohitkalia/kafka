package com.sunrise.kafka.service.consumer;

import java.io.IOException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.sunrise.kafka.service.ebpp.EBPPEventServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Consumer {
	
	@Autowired
	EBPPEventServiceImpl  eBPPEventServiceImpl;
	
	@KafkaListener(topics = "test", autoStartup = "false")
	public void onMessage(ConsumerRecord<Integer, String> record) throws IOException {
		log.info(" Record" +  record);	
		eBPPEventServiceImpl.processLibraryEvent(record);
	}

}