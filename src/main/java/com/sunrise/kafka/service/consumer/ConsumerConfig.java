package com.sunrise.kafka.service.consumer;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunrise.kafka.domain.EbillEvent;
import com.sunrise.kafka.service.producer.ProdcuerService;

import lombok.extern.slf4j.Slf4j;

@EnableKafka
@Configuration
@Slf4j
public class ConsumerConfig {
	
	@Autowired
	ProdcuerService  prodcuerService;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Bean	
	ConcurrentKafkaListenerContainerFactory<?, ?> kafkaListenerContainerFactory(
			ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
			ConsumerFactory<Object, Object> kafkaConsumerFactory) {
		ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();				
		configurer.configure(factory, kafkaConsumerFactory);		
		//Set Error Handler
		
		factory.setErrorHandler(((thrownException, data) -> {
			  log.info("customer error handler");
            log.info("Exception in consumerConfig is {} and the record is {}", thrownException.getMessage(), data);
            //persist
        }));
		
		
		// Setting the Retry Template
		factory.setRetryTemplate(retryTemplate());
		
//		//Setting the Recovery Call back
		factory.setRecoveryCallback(context ->{
			if(context.getLastThrowable().getCause() instanceof IllegalArgumentException) {
				//Implement the logic here
				ConsumerRecord<Integer, String> record = (ConsumerRecord<Integer, String>) context.getAttribute("record");
				log.info("Push Again " +record);
				pushAgain(record);
				
			}else {
				throw new RuntimeException(context.getLastThrowable().getMessage());
			}
			return null;
		});
		return factory;
	}

	private void pushAgain(ConsumerRecord<Integer, String> record) throws JsonMappingException, JsonProcessingException {
		EbillEvent ebillEvent = objectMapper.readValue(record.value(), EbillEvent.class);
		ebillEvent.getEbppEvent().setInvoiceID(007);
		prodcuerService.sendMessageToTheTopicWithProdcuerRecord(ebillEvent, "test");
		
	}

	private RetryTemplate retryTemplate() {
		RetryTemplate retryTemplate = new RetryTemplate();	
		FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
		fixedBackOffPolicy.setBackOffPeriod(1000); // Set Backoff Period
		
		retryTemplate.setRetryPolicy(simpleRetryPolicy());
		retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
		return retryTemplate;
	}

	private RetryPolicy simpleRetryPolicy() {
		//SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
		Map<Class<? extends Throwable>, Boolean> exception = new HashMap<Class<? extends Throwable>, Boolean>();
		exception.put(IllegalArgumentException.class, true);		
		exception.put(SocketTimeoutException.class, false);
		SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy(3,exception,true);
		return simpleRetryPolicy;
	}

	
}
