package com.sunrise.kafka.service.ebpp;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunrise.kafka.domain.EbillEvent;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EBPPEventServiceImpl {

	@Autowired
	ObjectMapper objectMapper;

	public void processLibraryEvent(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {
		EbillEvent ebillEvent = objectMapper.readValue(consumerRecord.value(), EbillEvent.class);
		validate(ebillEvent);
		log.info("ebillEvent : {} ", ebillEvent);

	}

	private void validate(EbillEvent ebillEvent) {	
		if (ebillEvent.getEbppEvent().getInvoiceID() == null) {
			log.info("Not valid Record");
			throw new IllegalArgumentException("Library Event Id is missing");
		}

	}

}
