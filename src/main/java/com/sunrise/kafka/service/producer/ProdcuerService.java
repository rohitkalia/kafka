package com.sunrise.kafka.service.producer;

import org.springframework.kafka.support.SendResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sunrise.kafka.domain.EbillEvent;

public interface ProdcuerService {
	
	void sendMessageDefault(EbillEvent ebppEvent) throws JsonProcessingException;
	void sendMessageToTheTopicAsync(EbillEvent ebppEvent, String topicName) throws JsonProcessingException;
	SendResult<Integer, String> sendMessageToTheTopicSync(EbillEvent ebppEvent, String topicNme) throws JsonProcessingException;
	void sendMessageToTheTopicWithProdcuerRecord(EbillEvent ebppEvent, String topicName) throws JsonProcessingException;
	

}
