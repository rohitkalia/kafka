package com.sunrise.kafka.service.producer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunrise.kafka.domain.EbillEvent;
import com.sunrise.kafka.exception.TopicUnknown;
import com.sunrise.kafka.service.admin.AdminService;

@Component
public class ProducerServiceImpl implements ProdcuerService {

	private static final Logger logger = LoggerFactory.getLogger(ProducerServiceImpl.class);

	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;

	@Autowired
	private AdminService adminService;

	@Autowired
	ObjectMapper objectMapper;

	/**
	 * This Producer 
	 */	
	@Override
	public void sendMessageDefault(EbillEvent ebppEvent) throws JsonProcessingException {

		// Sending message to kafka
		Integer key = ebppEvent.getEmailEventId();
		String value = objectMapper.writeValueAsString(ebppEvent);

		logger.info(String.format("#### -> Producing message -> %s", value));
		ListenableFuture<SendResult<Integer, String>> future = kafkaTemplate.sendDefault(key, value);

		future.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				System.out.println(
						"Sent message=[" + value + "] with offset=[" + result.getRecordMetadata().offset() + "]");
			}

			@Override
			public void onFailure(Throwable ex) {
				System.out.println("Unable to send message=[" + value + "] due to : " + ex.getMessage());
			}
		});

	}

	@Override
	public void sendMessageToTheTopicAsync(EbillEvent ebppEvent, String topicName) throws JsonProcessingException {
		
		// Sending message to kafka
		Integer key = ebppEvent.getEmailEventId();
		String value = objectMapper.writeValueAsString(ebppEvent);

		logger.info(String.format("#### -> Producing message -> %s", value));
		ListenableFuture<SendResult<Integer, String>> future = kafkaTemplate.send(topicName, key, value);

		future.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				System.out.println(
						"Sent message=[" + value + "] with offset=[" + result.getRecordMetadata().offset() + "]");
			}

			@Override
			public void onFailure(Throwable ex) {
				System.out.println("Unable to send message=[" + value + "] due to : " + ex.getMessage());
			}
		});

	}

	@Override
	public SendResult<Integer, String> sendMessageToTheTopicSync(EbillEvent ebppEvent, String topicName)
			throws JsonProcessingException {
	
		SendResult<Integer, String> result = null;
		// Sending message to kafka
		Integer key = ebppEvent.getEmailEventId();
		String value = objectMapper.writeValueAsString(ebppEvent);

		logger.info(String.format("#### -> Producing message -> %s", value));
		try {
			result = kafkaTemplate.send(topicName, key, value).get();			
			System.out.println(
					"Sent message=[" + value + "] with offset=[" + result.getRecordMetadata().offset() + "]");
			
		} catch (InterruptedException | ExecutionException ex) {
			System.out.println("Unable to send message=[" + value + "] due to : " + ex.getMessage());
		} catch (Exception e) {
			System.out.println("Unable to send message=[" + value + "] due to : " + e.getMessage());
		}

		return result;

	}

	/**
	 * Send message with Producer Record
	 */
	@Override
	public void sendMessageToTheTopicWithProdcuerRecord(EbillEvent ebppEvent, String topicName)
			throws JsonProcessingException {
	
		// Sending message to kafka
		Integer key = ebppEvent.getEmailEventId();
		String value = objectMapper.writeValueAsString(ebppEvent);
		// Creating the Producer Record
		ProducerRecord<Integer, String> producerRecord = buildProdcuerRecord(key, value, topicName);
		logger.info(String.format("#### -> Producing message -> %s", value));
		ListenableFuture<SendResult<Integer, String>> future = kafkaTemplate.send(producerRecord);

		future.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				System.out.println(
						"Sent message=[" + value + "] with offset=[" + result.getRecordMetadata().offset() + "]");
			}

			@Override
			public void onFailure(Throwable ex) {
				System.out.println("Unable to send message=[" + value + "] due to : " + ex.getMessage());
			}
		});

	}

	/**
	 * Create Producer Header
	 * 
	 * @param key
	 * @param value
	 * @param topic
	 * @return
	 */
	private ProducerRecord<Integer, String> buildProdcuerRecord(Integer key, String value, String topic) {
		List<Header> recordHeaders = Arrays.asList(new RecordHeader("soource", "ebpp-communication-ms".getBytes()));
		return new ProducerRecord<Integer, String>(topic, null, key, value, recordHeaders);
	}

}