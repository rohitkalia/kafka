package com.sunrise.kafka.service.admin;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.kafka.clients.admin.AdminClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class AdminServiceImpl implements AdminService {

	private static final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);

	private AdminClient adminClient;

	private Set<String> availableTopics = Collections.emptySet();

	@Override
	public boolean validateTopicName(String topicName) {
		return availableTopics.stream().anyMatch(topicName::equals);
	}

	@Scheduled(initialDelay = 2_000, fixedDelay = 90_000)
	private void updateTopics() {
		logger.info("update topics started");
		try {
			Set<String> freshSet = adminClient.listTopics().names().get(30L, TimeUnit.SECONDS);
			logger.info("Updated Set: " + freshSet.toString());
			setAvailableTopics(freshSet);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			logger.warn("update topics was interrupted");
		} catch (TimeoutException e) {
			e.printStackTrace();
			logger.warn("update topics has reach timeout");
		}
		logger.info("check topics completed");
	}

	private void setAvailableTopics(Set<String> freshSet) {
		synchronized (availableTopics) {
			availableTopics.clear();
			availableTopics = freshSet;
		}
	}

}
