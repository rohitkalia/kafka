package com.sunrise.kafka.service.admin;

public interface AdminService {

	boolean validateTopicName(String topicName);

}
